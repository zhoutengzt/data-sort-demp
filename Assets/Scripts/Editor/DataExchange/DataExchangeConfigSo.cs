using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;
[CreateAssetMenu(menuName = "MyConfig/DataExchangeConfig")]
public class DataExchangeConfigSo : SerializedScriptableObject
{
    [Title("数据交换位置")]
    [LabelText("区间1的起始位置")]
    [MinValue(0)]
    public int start1;
    [LabelText("区间2的起始位置")]
    [MinValue(0)]
    public int start2;
    [LabelText("区间长度")]
    [MinValue(1)]
    public int length;

    [Button(40, Name = "交换、导出CSV")]

    void Creat()
    {
        string path = Application.dataPath;
        path = path.Replace("Assets", "ExcleConvert/");
        var dirInfo = new DirectoryInfo(path);
        foreach (FileInfo preaInfoFile in dirInfo.GetFiles("*.*", SearchOption.AllDirectories))
        {
            if (preaInfoFile.Name.EndsWith(".xlsx"))
            {

                //构造Excel工具类
                ExcelUtility excel = new ExcelUtility(preaInfoFile.ToString());
                Encoding encoding = new UTF8Encoding();
                excel.ConvertToDataExchange(encoding);
            }

        }
        //Debug.Log("从Excle导出CSV成功");
        AssetDatabase.Refresh();
    }
}