using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class OpenDataExchangeConfig
{
    [MenuItem("MyTools/打开数据交换配置面板", false, 1)]
    public static void OpenPanel()
    {
        Selection.activeObject = DataExchangeConfig.Config;
    }
}
