using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataExchangeConfig
{
    private static DataExchangeConfigSo ConfigAssets = null;
    public static DataExchangeConfigSo Config
    {
        get
        {
            if (ConfigAssets == null)
            {
                ConfigAssets = (DataExchangeConfigSo)Resources.Load("DataExchangeConfig", typeof(DataExchangeConfigSo));
            }
            return ConfigAssets;
        }
    }
}
