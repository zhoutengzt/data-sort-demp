using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataSortConfig
{
    private static DataSortConfigSo ConfigAssets = null;
    public static DataSortConfigSo Config
    {
        get
        {
            if (ConfigAssets == null)
            {
                ConfigAssets = (DataSortConfigSo)Resources.Load("DataSortConfig", typeof(DataSortConfigSo));
            }
            return ConfigAssets;
        }
    }
}
