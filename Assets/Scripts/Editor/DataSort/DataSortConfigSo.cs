using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;
[CreateAssetMenu(menuName = "MyConfig/DataSortConfig")]
public class DataSortConfigSo : SerializedScriptableObject
{
    [Title("按人均次数重新分组")]
    [MinValue(1)]
    public int GroupNum = 10;

    [Button(40,Name ="导出CSV")]

    void Creat()
    {
        string path = Application.dataPath;
        path = path.Replace("Assets", "ExcleConvert/");
        List<string> excels = new List<string>();
        var dirInfo = new DirectoryInfo(path);
        foreach (FileInfo preaInfoFile in dirInfo.GetFiles("*.*", SearchOption.AllDirectories))
        {
            if (preaInfoFile.Name.EndsWith(".xlsx"))
            {

                //构造Excel工具类
                ExcelUtility excel = new ExcelUtility(preaInfoFile.ToString());
                Encoding encoding = new UTF8Encoding();
                //excel.ConvertToList<string>();
                excel.ConvertToDataSort(encoding);
                excels.Add(preaInfoFile.Name.Split('.')[0]);
            }

        }
        //Debug.Log("从Excle导出Json成功");
        AssetDatabase.Refresh();
    }
}
