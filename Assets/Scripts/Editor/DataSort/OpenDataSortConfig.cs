using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class OpenDataSortConfig 
{
    [MenuItem("MyTools/打开数据排序配置面板", false, 1)]
    public static void OpenPanel()
    {
        Selection.activeObject = DataSortConfig.Config;
    }
}
