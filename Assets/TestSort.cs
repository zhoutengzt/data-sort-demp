using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TestSort
{
    public class Services
    {
        /// <summary>
        /// 权重
        /// </summary>
        public int Weight { get; set; }

        /// <summary>
        /// 权重计算
        /// </summary>
        public int WeightCalc { get; set; }

        /// <summary>
        /// 权重分数
        /// </summary>
        public float WeightScore { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 做个记录
        /// </summary>
        public List<float> ScopeRecord { get; set; } = new List<float>();


        public List<Dictionary<string, object>> TableDic = new List<Dictionary<string, object>>();
    }


    // Start is called before the first frame update
    void Start()
    {
        // Sort2();
        // Sort3();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public static List<Dictionary<string, object>> DataSort(List<Dictionary<string, object>> dataList, int servicesCount)
    {

        List<Dictionary<string, object>> tableDic = new List<Dictionary<string, object>>();
        //需要平均分配的数据  22,66,4,7,863
        List<Services> Service = new List<Services>();


        //生成服务器随机数据
        for (int i = 0; i < servicesCount; i++)
        {
            var weight = servicesCount;//new System.Random().Next(1, servicesCount);
            Service.Add(new Services()
            {
                Id = i,
                Weight = weight, /*new Random().Next(1, servicesCount),*/ //随机权重
                WeightCalc = weight,
                WeightScore = 0
            });
        }

        //数据组排序
        dataList = dataList.OrderByDescending(q => { object t; q.TryGetValue("UserPlayAvg", out t); return float.Parse(t.ToString()); }).ToList();


        //轮询拿数据组数值  每轮询一次 根据权重分数重新分配先拿的人 且先拿高分数
        //因为是数据组是降序最后分配不够拿时按权重分配也会将差距缩小

        //用数据组进行轮询  也可以用服务器组轮询
        foreach (var scope in dataList)
        {
            //可优化
            var empty = Service.OrderBy(q => q.WeightScore / q.Weight).FirstOrDefault(q => q.WeightCalc > 0);
            object t;
            scope.TryGetValue("UserPlayAvg", out t);
            empty.WeightScore += float.Parse(t.ToString());
            empty.ScopeRecord.Add(float.Parse(t.ToString()));
            empty.TableDic.Add(scope);
            empty.WeightCalc--;

            if (Service.Count(q => q.WeightCalc > 0) <= 0)
            {
                Service.ForEach(q => { q.WeightCalc = q.Weight; });
            }
        }

        //打印服务器数据
        foreach (var item in Service.OrderBy(q => q.Id))
        {
            //Debug.Log($"服务器号：{item.Id},服务器权重：{item.Weight},服务器分数：{item.WeightScore}");
            //Debug.Log($"分数记录：{string.Join(",", item.ScopeRecord)}");

            tableDic.AddRange(item.TableDic);
            tableDic.Add(new Dictionary<string, object>());
        }

        ////打印数据组数据
        //int colum = 10;
        //Debug.Log("数据组数据：");
        //for (int i = 0; i <= dataList.Count / colum; i++)
        //{
        //    Debug.Log($"{string.Join(",", dataList.Skip(i * colum).Take(colum))}");
        //}

        return tableDic;

    }

    public static void Sort3(List<float> DataCount, int servicesCount)
    {
        //需要平均分配的数据  22,66,4,7,863
        List<Services> Service = new List<Services>();


        //生成服务器随机数据
        for (int i = 0; i < servicesCount; i++)
        {
            var weight = new System.Random().Next(1, servicesCount);
            Service.Add(new Services()
            {
                Id = i,
                Weight = weight, /*new Random().Next(1, servicesCount),*/ //随机权重
                WeightCalc = weight,
                WeightScore = 0
            });
        }

        //数据组排序
        DataCount = DataCount.OrderByDescending(q => q).ToList();


        //轮询拿数据组数值  每轮询一次 根据权重分数重新分配先拿的人 且先拿高分数
        //因为是数据组是降序最后分配不够拿时按权重分配也会将差距缩小

        //用数据组进行轮询  也可以用服务器组轮询
        foreach (var scope in DataCount)
        {
            //可优化
            var empty = Service.OrderBy(q => q.WeightScore / q.Weight).FirstOrDefault(q => q.WeightCalc > 0);

            empty.WeightScore += scope;
            empty.ScopeRecord.Add(scope);
            empty.WeightCalc--;

            if (Service.Count(q => q.WeightCalc > 0) <= 0)
            {
                Service.ForEach(q => { q.WeightCalc = q.Weight; });
            }
        }

        //打印服务器数据
        foreach (var item in Service.OrderBy(q => q.Id))
        {
            //Debug.Log($"服务器号：{item.Id},服务器权重：{item.Weight},服务器分数：{item.WeightScore}");
            //Debug.Log($"分数记录：{string.Join(",", item.ScopeRecord)}");

        }

        //打印数据组数据
        int colum = 10;
        Debug.Log("数据组数据：");
        for (int i = 0; i <= DataCount.Count / colum; i++)
        {
            Debug.Log($"{string.Join(",", DataCount.Skip(i * colum).Take(colum))}");
        }
    }

    private void Sort2()
    {
        int[] nums = new int[100];
        int[][] groups = new int[10][];
        for (int i = 0; i < 10; i++)
        {
            groups[i] = new int[10];
        }

        for (int i = 0; i < 100; i++)
        {
            nums[i] = i + 1;
        }
        Array.Sort(nums);

        // 将100个数分成10组
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                groups[i][j] = nums[j * 10 + i];
            }
        }
        for (int i = 0; i < 10; i++)
        {
            int sum = 0;
            for (int j = 0; j < groups[i].Length; j++)
            {
                sum += groups[i][j];
            }
            Debug.Log("Group " + (i + 1) + ": " + string.Join(", ", groups[i]));
            Debug.Log("Group " + (i + 1) + ": SUM=" + sum);
            Debug.Log("Group " + (i + 1) + ": AVG=" + sum / 10);
        }
    }

    private void Sort()
    {
        int[] nums = new int[100];


        for (int i = 0; i < 100; i++)
        {
            nums[i] = i;//UnityEngine.Random.Range(1, 101);
        }

        Array.Sort(nums);

        int[][] groups = new int[10][];
        for (int i = 0; i < 10; i++)
        {
            groups[i] = new int[10];
        }

        int[] averages = new int[10];

        for (int i = 0; i < 10; i++)
        {
            int sum = 0;
            for (int j = 0; j < 10; j++)
            {
                groups[i][j] = nums[i * 10 + j];
                sum += nums[i * 10 + j];
            }
            averages[i] = sum / 10;
        }

        for (int i = 0; i < 90; i++)
        {
            int minDiff = int.MaxValue;
            int minGroup = -1;

            for (int j = 0; j < 10; j++)
            {
                int curDiff = Math.Abs(averages[j] - nums[i]);

                if (curDiff < minDiff)
                {
                    minDiff = curDiff;
                    minGroup = j;
                }
            }

            int minDiff2 = int.MaxValue;
            int minIndex = -1;

            for (int j = i + 1; j < 100; j++)
            {
                int curDiff = Math.Abs(averages[minGroup] + nums[j] - averages[minGroup] * 11) - Math.Abs(averages[minGroup] - nums[i]);

                if (curDiff < minDiff2)
                {
                    minDiff2 = curDiff;
                    minIndex = j;
                }
            }

            groups[minGroup][i % 10] = nums[i];
            groups[minGroup][(i + 1) % 10] = nums[minIndex];

            averages[minGroup] = (averages[minGroup] * 10 + nums[i] + nums[minIndex]) / 12;

            nums[i] = 0;
            nums[minIndex] = 0;
        }

        for (int i = 0; i < 10; i++)
        {
            Debug.Log("Group " + (i + 1) + ": " + string.Join(", ", groups[i]));
        }

    }

    public static void Test2()
    {
        int[] nums = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }; // 定义一个长度为10的数组

        int n = 2; // 分成2个数组
        int m = nums.Length; // 数组的长度

        int[,] dp = new int[n + 1, m + 1]; // 定义一个n+1行，m+1列的二维数组

        // 计算数组的总和
        int sum = 0;
        for (int i = 0; i < nums.Length; i++)
        {
            sum += nums[i];
        }

        // 计算每个数组的目标和
        int target = sum / n;

        // 计算前缀和
        int[] prefixSum = new int[m + 1];
        for (int i = 1; i <= m; i++)
        {
            prefixSum[i] = prefixSum[i - 1] + nums[i - 1];
        }

        // 初始化dp数组
        for (int i = 1; i <= n; i++)
        {
            for (int j = i; j <= m; j++)
            {
                dp[i, j] = int.MaxValue;
            }
        }

        // 动态规划
        for (int i = 1; i <= n; i++)
        {
            for (int j = i; j <= m; j++)
            {
                for (int k = i - 1; k < j; k++)
                {
                    int val = Math.Max(dp[i - 1, k], prefixSum[j] - prefixSum[k]);
                    dp[i, j] = Math.Min(dp[i, j], val);
                }
            }
        }

        // 输出结果
        for (int i = 1; i <= n; i++)
        {
            for (int j = i; j <= m; j++)
            {
                if (dp[i, j] == target)
                {
                    Debug.Log($"Array {i}: {string.Join(",", nums[j - 1], nums[j - 2], nums[j - 3])}");
                    break;
                }
            }
        }

        Debug.Log("Console");
    }

    public static List<int[]> SplitArray(int[] array, int n)
    {
        List<int[]> result = new List<int[]>();

        int length = array.Length;
        int size = length / n;
        int remainder = length % n;

        Array.Sort(array); // 对数组进行排序

        int index = 0;
        for (int i = 0; i < n; i++)
        {
            int[] tempArray;
            if (remainder > 0)
            {
                tempArray = new int[size + 1];
                remainder--;
            }
            else
            {
                tempArray = new int[size];
            }

            int tempSum = 0;
            for (int j = 0; j < tempArray.Length; j++)
            {
                tempArray[j] = array[index++];
                tempSum += tempArray[j];
            }

            int avg = tempSum / tempArray.Length;
            if (remainder > 0 && array[index] - avg < avg - array[index - tempArray.Length])
            {
                tempArray[tempArray.Length - 1] = array[index++];
            }

            result.Add(tempArray);
        }

        return result;
    }

    public static void Test3()
    {
        int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9 }; //输入的数组
        int n = 3; //要分成的数组个数

        int m = arr.Length;
        int[,] dp = new int[n + 1, m + 1];
        int[,] path = new int[n + 1, m + 1];

        for (int i = 1; i <= n; i++)
        {
            for (int j = i; j <= m; j++)
            {
                if (i == 1)
                {
                    dp[i, j] = dp[i, j - 1] + arr[j - 1];
                    path[i, j] = j - 1;
                }
                else
                {
                    dp[i, j] = int.MaxValue;
                    for (int k = i - 1; k < j; k++)
                    {
                        int val = Math.Max(dp[i - 1, k], dp[1, j] - dp[1, k]);
                        if (val < dp[i, j])
                        {
                            dp[i, j] = val;
                            path[i, j] = k;
                        }
                    }
                }
            }
        }

        int[] res = new int[n];
        int index = n;
        int pos = m;
        while (index > 0)
        {
            res[index - 1] = path[index, pos];
            pos = path[index, pos];
            index--;
        }
        Debug.Log(string.Join(",", arr));
        //for (int i = 0; i < n; i++)
        //{
        //    Debug.Log("[ ");
        //    for (int j = res[i]; j < res[i + 1]; j++)
        //    {
        //        Debug.Log(arr[j] + " ");
        //    }
        //    Debug.Log("]");
        //}
    }
}

